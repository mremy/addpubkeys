addpubkeys
==========


Ansible role to manage public key in authorized keys files for ssh.

Somehow the design is bad, as you can't remove a key without removing
existing file. Maybe with lineinfile or template ?

Status
------

Experimental

Don't use on production environnement.

Requirements
------------

The ssh serveur configuration is not covered by this module. You should
ensure "elsewhere" that the file you create is accepted as an authorized
keys file.

Reminder: this is usually managed by the AuthorizedKeysFile parameter in
the /etc/ssh/sshd\_config file on most Linux distribution.

From my own test, you can only have one parameter, but more than 1 file
definition is possible (3 tested ok).

TODO: Check in the openssh source code.

Role Variables
--------------

Variables are for root and "standart" values per default.

Here are the role variables:

ssh\_authorized\_keys\_file: the file to write the keys in, default to:
'/etc/ssh/authorized\_keys/root'.

remove\_present: if no, keep the file for the authorized keys instead of
erasing content, default to 'yes'.

ssh\_user: the user, default to 'root'.

all\_hosts\_keys: the list of keys to be added.

TODO: better variables name.

Dependencies
------------

It uses the authorized\_key type, which seems to be designed with the personnal
user authorized\_key file in mind, as it change the ownership of the file.
Or maybe the user parameter is useless in the case of file outside of user
files (otherwise he would be allowed to change the content if it is writable).

Example Playbook
----------------

TODO:

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
